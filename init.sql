CREATE EXTENSION ltree;


CREATE SEQUENCE tree_id START 9;


CREATE TABLE section (
  id integer primary key default NEXTVAL('tree_id'),
  text varchar(250) not null,
  path ltree
);

INSERT INTO section(id, text, path) VALUES (1, 'Тестовый текст', 'root');
INSERT INTO section(id, text, path) VALUES (2, 'Просто текст', 'root.1');
INSERT INTO section(id, text, path) VALUES (3, 'Еще один текст', 'root');
INSERT INTO section(id, text, path) VALUES (4, 'А здесь нет слова text', 'root.1.2');
INSERT INTO section(id, text, path) VALUES (5, 'Снова текст', 'root.3');
INSERT INTO section(id, text, path) VALUES (6, 'Я знаю три слова', 'root.1.2');
INSERT INTO section(id, text, path) VALUES (7, 'Тест тест тест', 'root');
INSERT INTO section(id, text, path) VALUES (8, 'Ура', 'root.3.5');

CREATE INDEX path_gist_idx ON section USING gist(path);
CREATE INDEX path_idx ON section USING btree(path);

CREATE INDEX text_fulltext_idx ON section USING GIN (to_tsvector('russian', text));



CREATE SEQUENCE user_id START 1;

CREATE TABLE "user" (
  id integer primary key default NEXTVAL('user_id'),
  login varchar(50) UNIQUE not null,
  password varchar(100) not null
);


-- user admin:admin
INSERT INTO "user"(login, password) VALUES ('admin', '$5$rounds=535000$B2uww4P0O44XFPfa$1F8r8W/66gms7LXPmyq9NyrkcgEWHKRIVyZFiaTFowD');