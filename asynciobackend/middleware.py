
from aiohttp import web
from aiohttp_session import get_session


async def authorize_middleware(app, handler):
    async def middleware(request):
        def check_path(path):
            result = True
            for r in ['/api/login', '/api/logout', '/api/doc']:
                if path.startswith(r):
                    result = False
            return result

        session = await get_session(request)

        if session.get('user'):
            return await handler(request)
        elif check_path(request.path):
            raise web.HTTPUnauthorized()
        else:
            return await handler(request)

    return middleware



def setup_middlewares(app):
    app.middlewares.append(authorize_middleware)