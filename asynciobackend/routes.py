import pathlib

from asynciobackend.views import *


PROJECT_ROOT = pathlib.Path(__file__).parent.parent


def setup_routes(app):
    app.router.add_get('/api/nodes/search/{keywords}', node_search, name='node_search', allow_head=False)
    app.router.add_get('/api/nodes/{id:\d+}', node, name='node', allow_head=False)
    app.router.add_post('/api/nodes/{id:\d+}', node_insert, name='node_insert')

    app.router.add_post('/api/login', login, name='login')
    app.router.add_get('/api/logout', logout, name='logout', allow_head=False)

    app.router.add_static('/static/', path=str(PROJECT_ROOT / 'static'), name='static')
