import asyncpg



async def init_pg(app):
    conf = app['config']['postgres']

    app['db_pool'] = await asyncpg.create_pool(**conf)


async def close_pg(app):
    app['db_poll'].close()


