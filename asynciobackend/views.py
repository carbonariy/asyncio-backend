
from aiohttp import web
from aiohttp_session import get_session

from passlib.hash import sha256_crypt


__all__ = ['node', 'node_search', 'node_insert', 'login', 'logout']



async def node(request):
    """
    ---
    description: Gets the node by id and returns ids of all parent nodes
    tags:
    - Get node by id
    produces:
    - application/json
    parameters:
    - in: path
      name: id
      required: true
      type: integer
      description: ID of the node
    responses:
      200:
        description: OK
      404:
        description: Not found. Can't find the node
    """
    async with request.app['db_pool'].acquire() as conn:
        id = request.match_info['id']

        row = await conn.fetchrow('SELECT * FROM section WHERE id = $1', int(id))

        if row:
            path_split = [ int(id) for id in row['path'].split('.')[1:] ]
        else:
            raise web.HTTPNotFound(text="Can't find the node by id '{0}'".format(id))

        return web.json_response(path_split)



async def node_search(request):
    """
    ---
    summary: Performs full-text search the node by the keywords and returns ids of all parent nodes
    tags:
    - Search
    produces:
    - application/json
    parameters:
    - in: path
      name: keywords
      required: true
      type: string
      description: Search keywords
    responses:
      200:
        description: OK
      404:
        description: Not found. Can't find the node
    """
    async with request.app['db_pool'].acquire() as conn:
        keywords = request.match_info['keywords']

        row = await conn.fetchrow("SELECT * FROM section WHERE text @@ to_tsquery(quote_literal($1))", keywords)

        if row:
            path_split = [ int(id) for id in row['path'].split('.')[1:] ]
        else:
            raise web.HTTPNotFound(text="Can't find the node for the keywords '{0}'".format(keywords))

        return web.json_response(path_split)



async def node_insert(request):
    """
    ---
    summary: Inserts node and returns it
    tags:
    - Insert node
    produces:
    - application/json
    parameters:
    - in: path
      name: id
      required: true
      type: integer
      description: Parent node's id
    - in: formData
      name: text
      required: true
      type: string
      description: Node's name
    responses:
      200:
        description: OK
      400:
        description: Bad request. You have not specified text value
      404:
        description: Not found. Can't find the node
    """
    id = request.match_info['id']

    data = await request.post()

    try:
        text = data['text']
    except (KeyError, TypeError, ValueError) as e:
        raise web.HTTPBadRequest(text='You have not specified text value') from e

    async with request.app['db_pool'].acquire() as conn:
        async with conn.transaction():
            row = await conn.fetchrow('SELECT path FROM section WHERE id = $1', int(id))

            if not row:
                raise web.HTTPNotFound(text="Can't find the node by id '{0}'".format(id))

            result = await conn.fetchrow('''
              INSERT INTO section(id, text, path) VALUES(DEFAULT, $1, $2) RETURNING id, text, path
            ''', text, '{0}.{1}'.format(row['path'], id))

        return web.json_response(dict(result))


async def login(request):
    """
    ---
    summary: Logins user
    tags:
    - Login user
    produces:
    - application/json
    parameters:
    - in: formData
      name: login
      required: true
      type: string
      description: User login
    - in: formData
      name: password
      required: true
      type: string
      description: User password
    responses:
      200:
        description: OK
      400:
        description: Bad request. You have not specified login or password value
    """
    data = await request.post()

    try:
        login = data['login']
        password = data['password']
    except (KeyError, TypeError, ValueError) as e:
        raise web.HTTPBadRequest(text='You have not specified login or password value') from e

    async with request.app['db_pool'].acquire() as conn:
        is_verified = False

        async with conn.transaction():
            row = await conn.fetchrow('SELECT password FROM "user" WHERE login = $1', login)

            if not row:
                raise web.HTTPUnauthorized()

            is_verified = sha256_crypt.verify(password, row['password'])

        if is_verified:
            session = await get_session(request)
            session['user'] = login

            return web.json_response({})
        else:
            raise web.HTTPUnauthorized()


async def logout(request):
    """
    ---
    summary: Logouts user
    tags:
    - Logout user
    produces:
    - application/json
    responses:
      200:
        description: OK
    """
    async with request.app['db_pool'].acquire() as conn:
        session = await get_session(request)

        session.invalidate()

        return web.json_response({})