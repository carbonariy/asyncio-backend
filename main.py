import argparse
import asyncio
import logging
import sys

from aiohttp import web
from aiohttp_swagger import setup_swagger
from aiohttp_session import get_session, setup as setup_session
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from trafaret_config import commandline

from asynciobackend.utils import TRAFARET
from asynciobackend.db import close_pg, init_pg
from asynciobackend.routes import setup_routes
from asynciobackend.middleware import setup_middlewares




def init(loop, argv):
    ap = argparse.ArgumentParser()
    commandline.standard_argparse_options(ap, default_config='config.yaml')

    options = ap.parse_args(argv)

    config = commandline.config_from_options(options, TRAFARET)

    # setup application and extensions
    app = web.Application(loop=loop)

    # load config from yaml file in current dir
    app['config'] = config

    # setup Jinja2 template renderer
    #aiohttp_jinja2.setup(
    #    app, loader=jinja2.PackageLoader('aiohttpdemo_polls', 'templates'))

    # create connection to the database
    app.on_startup.append(init_pg)

    # shutdown db connection on exit
    app.on_cleanup.append(close_pg)

    # setup views and routes
    setup_session(app, EncryptedCookieStorage(app['config']['secret_key']))
    setup_routes(app)
    setup_middlewares(app)
    setup_swagger(app,
                  title='Tree API',
                  description='Simple test API',
                  api_version='0.0.1',
                  swagger_url="/api/doc")

    return app


def main(argv):
    # init logging
    logging.basicConfig(level=logging.DEBUG)

    loop = asyncio.get_event_loop()

    app = init(loop, argv)
    web.run_app(app,
                host=app['config']['host'],
                port=app['config']['port'])


if __name__ == '__main__':
    main(sys.argv[1:])